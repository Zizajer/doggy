﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pole : SpawnableObject
{
    Rigidbody2D rb2d;
    ObjectManager objectManager;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        objectManager = GameObject.FindObjectOfType<ObjectManager>();
    }

    // Update is called once per frame
    void Update()
    {
        Move(rb2d);
    }

    void OnBecameInvisible()
    {
        objectManager.DecrementObjectAmount();
        Destroy(gameObject);
    }
}
