﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundScroller : MonoBehaviour
{

    public float ScrollingSpeed;
    public float clampPosition;

    private Vector3 startPosition;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float newPosition = Mathf.Repeat(Time.time * ScrollingSpeed, clampPosition);
        transform.position = startPosition + Vector3.up * newPosition;
    }


}
