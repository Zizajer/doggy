﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doggy : MonoBehaviour
{
    GameManager gameManager;

    [SerializeField]
    int health;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            gameManager.EndGame();
        }
    }

    public int GetHealth()
    {
        return health;
    }

    public void DecreaseHealth(int damage)
    {
        health = health - damage;
    }
}
