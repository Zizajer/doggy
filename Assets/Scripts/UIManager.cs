﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject GameOverPanel;

    public void SetGameOverPanel()
    {
        GameOverPanel.SetActive(true);
    }

    public void RestartLevel()
    {
        string sceneToReplay = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneToReplay);
    }
}
