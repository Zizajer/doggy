﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    int objectAmount;

    public int objectMaxAmount;

    // Start is called before the first frame update
    void Start()
    {
        objectAmount = 0;
    }

    private void Update()
    {
        Debug.Log(objectAmount);
    }

    public int GetObjectAmount()
    {
        return objectAmount;
    }

    public void IncrementObjectAmount()
    {
        objectAmount++;
    }

    public void DecrementObjectAmount()
    {
        objectAmount--;
    }
}
