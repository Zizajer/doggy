﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    Transform spawnerPosGizmo;

    public GameObject[] prefabList;

    public float spawnRange;

    // Start is called before the first frame update
    void Start()
    {
        spawnerPosGizmo = this.gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(spawnerPosGizmo.position, new Vector3(spawnerPosGizmo.position.x + spawnRange, spawnerPosGizmo.position.y, spawnerPosGizmo.position.z));
    }
}
