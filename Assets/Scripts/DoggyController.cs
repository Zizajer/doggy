﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoggyController : MonoBehaviour
{
    Rigidbody2D rb2d;
    Vector2 targetPos;

    [HideInInspector]
    public Kupa nearestKupa;
    public Rigidbody2D defaultTargetPos;
    public UIManager UIManager;

    bool isKupaInRange;

    public float defaultDoggySpeed;
    public float kupaDoggySpeed;
    float doggySpeed;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        isKupaInRange = false;
    }

    private void Update()
    {
        if (nearestKupa != null)
        {
            isKupaInRange = true;
            doggySpeed = kupaDoggySpeed;
        }
        else
        {
            isKupaInRange = false;
            doggySpeed = defaultDoggySpeed;
        }

    }
    //Jeszcze to trzeba dokończyć
    void FixedUpdate()
    {
        if (!isKupaInRange)
        {
            targetPos = defaultTargetPos.position;
        }
        else
        {
            Rigidbody2D nearestKupaRb2d = nearestKupa.gameObject.GetComponent<Rigidbody2D>();
            targetPos = nearestKupaRb2d.position;
        }
        rb2d.MovePosition(rb2d.position + targetPos * doggySpeed * Time.deltaTime);
        
    }
}
