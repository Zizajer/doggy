﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultTargetPosManager : MonoBehaviour
{
    public float speed;
    public float changePosRate;
    public float targetPosRange;
    Vector2 targetPos;
    Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        InvokeRepeating("ChangePos", 0, changePosRate);
    }

    void FixedUpdate() => rb2d.MovePosition
    (rb2d.position +
        ((targetPos - rb2d.position).normalized * speed * Time.deltaTime));
    

    void ChangePos()
    {
        targetPos = new Vector2(Random.Range(-targetPosRange, targetPosRange), gameObject.transform.position.y);
        Debug.Log(targetPos); 
    }
}
