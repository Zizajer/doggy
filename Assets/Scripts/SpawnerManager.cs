﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    Transform spawnerPos;
    ObjectManager objectManager;
    Spawner activeSpawner;

    public float spawnWait;
    public float startWait;
    public GameObject[] spawnersList;

    int randomSpawner;
    int randomPrefab;

    // Start is called before the first frame update
    void Start()
    {
        objectManager = GameObject.FindObjectOfType<ObjectManager>();
        Invoke("Spawn", startWait);

    }

    // Update is called once per frame
    void Update()
    {

    }

    void Spawn()
    {
        while (objectManager.GetObjectAmount() < objectManager.objectMaxAmount)
        {
            randomSpawner = Random.Range(0, spawnersList.Length);
            spawnerPos = spawnersList[randomSpawner].transform;
            activeSpawner = spawnersList[randomSpawner].GetComponent<Spawner>();
            randomPrefab = Random.Range(0, activeSpawner.prefabList.Length);
            Vector3 spawnPos = new Vector3(Random.Range(spawnerPos.position.x, spawnerPos.position.x + activeSpawner.spawnRange), spawnerPos.position.y, spawnerPos.position.z);
            Instantiate(activeSpawner.prefabList[randomPrefab], spawnPos, Quaternion.identity);
            objectManager.IncrementObjectAmount();
            Invoke("Spawn", spawnWait);
            return;
        }
        Invoke("Spawn", spawnWait);
    }

    
}
