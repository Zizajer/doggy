﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkController : MonoBehaviour
{
    public GameManager GameManager;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "CollisionObcjet")
        {
            GetComponent<DistanceJoint2D>().enabled = false;
            GameManager.Invoke("EndGame",2f);
        }
    }
}
