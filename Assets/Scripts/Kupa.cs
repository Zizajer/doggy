﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kupa : SpawnableObject
{
    ObjectManager objectManager;
    Rigidbody2D rb2d;
    DoggyController doggyCon;
    Doggy doggy;
    public LayerMask doggyLayerMask;

    bool isEating;

    public float eatingRadius;
    public int kupaDamage;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        doggy = GameObject.FindObjectOfType<Doggy>();
        doggyLayerMask = 1 << LayerMask.NameToLayer ("Doggy");
        objectManager = GameObject.FindObjectOfType<ObjectManager>();
    }

    void Update()
    {
        isEating = Physics2D.OverlapCircle(this.gameObject.transform.position, eatingRadius, doggyLayerMask);

        if (isEating)
        {
            doggy.DecreaseHealth(kupaDamage);
            Destroy(this.gameObject);
            // Tutaj cos co ma sie dziać po zjedzeniu kupy
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Move(rb2d);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Doggy"))
        {
            doggyCon = collision.gameObject.GetComponent<DoggyController>();
            doggyCon.nearestKupa = this;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Doggy"))
        {
            doggyCon = collision.gameObject.GetComponent<DoggyController>();
            doggyCon.nearestKupa = null;
        }
    }

    void OnBecameInvisible()
    {
        objectManager.DecrementObjectAmount();
        Destroy(gameObject);
        doggyCon.nearestKupa = null;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.gameObject.transform.position, eatingRadius);
    }
}
