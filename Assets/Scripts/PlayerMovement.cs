﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    float deltaX, deltaY;
    bool moveAllowed;

    Rigidbody2D rb2d;


    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        moveAllowed = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
            
            if (GetComponent<Collider2D>().OverlapPoint(touchPos) && touch.phase == TouchPhase.Began)
            {
                deltaX = touchPos.x - transform.position.x;
                deltaY = touchPos.y - transform.position.y;
            }

            if (GetComponent<Collider2D>().OverlapPoint(touchPos) && touch.phase == TouchPhase.Moved)
                rb2d.MovePosition(new Vector2(touchPos.x - deltaX, touchPos.y - deltaY));

              
        }
    }
}
