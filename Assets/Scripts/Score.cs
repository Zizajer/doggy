﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
    Doggy doggy;

    public Text scoreText;
    public Text healthText;
    public float scoreInscreasedEverySec;

    float scoreAmount;

    // Start is called before the first frame update
    void Start()
    {
        scoreAmount = 0;
        doggy = GameObject.FindObjectOfType<Doggy>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + (int)scoreAmount;
        scoreAmount += scoreInscreasedEverySec * Time.deltaTime;
        healthText.text = "Health: " + doggy.GetHealth();
    }
}
