﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool isGameOver;
    string sceneToReplay;

    public UIManager UIManager;

    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
        Time.timeScale = 1;
    }

    public void EndGame()
    {
        if (!isGameOver)
        {
            isGameOver = true;
            Time.timeScale = 0;
            UIManager.SetGameOverPanel();
        }
    }

    public bool IsGameOver()
    {
        return isGameOver;
    }
}
