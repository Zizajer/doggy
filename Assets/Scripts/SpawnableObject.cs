﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnableObject : MonoBehaviour
{

    public float speed;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void Move(Rigidbody2D rb2d)
    {
        rb2d.MovePosition(rb2d.position + new Vector2(0, speed));
    }
}
